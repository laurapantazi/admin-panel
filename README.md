# Admin Panel

## Description
This project was bootstrapped with Create React App. React components were separated into two different directories: presentational components (components) and container components (containers).
Redux library was used for state management.
For styling, react-bootstrap and Sass were added.

## Features
* [x] React
* [x] Redux
* [x] React-Bootstrap, Bootstrap
* [x] Sass
* [x] BEM for CSS naming

## Getting Started
Clone or download repository

### Install dependencies
`npm install` or `yarn install`

### Start server
`npm start` or `yarn start`

### Start tests
`npm test` or `yarn test`

### Build
`npm run build` or `yarn run build`

### Eject
`npm run eject` or `yarn run eject`