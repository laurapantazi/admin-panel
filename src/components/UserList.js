import React from 'react'
import {ListGroup, Row, Col, Image} from 'react-bootstrap'
import PropTypes from 'prop-types'

class UserList extends React.Component {
  componentDidMount () {
    this.props.fetchUsers()
  }

  getUserEmail (email) {
    if (email) return (<p className="list-item__text--secondary">{email}</p>)
    else return
  }

  getListUser () {
    return this.props.users.map(user => 
      <ListGroup.Item as="button" key={user.id} className="list-item" onClick={()=> this.props.selectedUserId(user.id)}>
        <Row>
          <Col xs={12} sm={12} md={4} lg={3} xl={2}>
            <Image className="list-item__image" style={{ width: 75, height: 'auto' }} src={user.photo} roundedCircle alt="User profile image." />
          </Col>
          <Col md={8} lg={9} xl={10} className="d-none d-sm-none d-md-flex list-item__text">
            <p className="list-item__text--primary">{user.name}</p>    
            {this.getUserEmail(user.email)}
          </Col>
        </Row>
      </ListGroup.Item>
    )
  }
  render() {
    return (
      <ListGroup as="div" variant="flush" className="list">
        {this.getListUser()}
      </ListGroup>
    )
  }
}

UserList.propTypes = {
  users: PropTypes.array,
  getUsers: PropTypes.func.isRequired,
  fetchUsers: PropTypes.func.isRequired,
  selectedUserId: PropTypes.func.isRequired
}
export default UserList