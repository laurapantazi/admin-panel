import React from 'react'
import {Form, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'

class UserForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      id: '',
      name: '',
      email: '',
      phone: '',
      address: '',
      company: '',
      isDisabledSaveBtn: false,
      isVisibleCancelBtn: false
    }
  }
  componentDidMount () {
    this.setState({
      id: (this.props.selectedUser && this.props.selectedUser.id) ? this.props.selectedUser.id : '',
      name: (this.props.selectedUser && this.props.selectedUser.name) ? this.props.selectedUser.name : '',
      email: (this.props.selectedUser && this.props.selectedUser.email) ? this.props.selectedUser.email : '',
      phone: (this.props.selectedUser && this.props.selectedUser.phone) ? this.props.selectedUser.phone : '',
      address: (this.props.selectedUser && this.props.selectedUser.address) ? this.props.selectedUser.address : '',
      company: (this.props.selectedUser && this.props.selectedUser.company) ? this.props.selectedUser.company : ''
    })
  }
  componentDidUpdate(prevProps) {
    if (this.props.selectedUserId !== prevProps.selectedUserId) {
      this.setState({
        id: this.props.selectedUser.id || '',
        name: this.props.selectedUser.name || '',
        email: this.props.selectedUser.email || '',
        phone: this.props.selectedUser.phone || '',
        address: this.props.selectedUser.address || '',
        company: this.props.selectedUser.company || ''
      })
    }
  }

  handleInputChange = event => {
    let {name, value} = event.target
    this.setState({
      [name]: value,
      isDisabledSaveBtn: false,
      isVisibleCancelBtn: true
    })
  }
  handleCancelBtn = () => {
    this.setState({
      id: this.props.selectedUser.id || '',
      name: this.props.selectedUser.name || '',
      email: this.props.selectedUser.email || '',
      phone: this.props.selectedUser.phone || '',
      address: this.props.selectedUser.address || '',
      company: this.props.selectedUser.company || '',
      isDisabledSaveBtn: true,
      isVisibleCancelBtn: false
    })
  }
  handleSubmit = event => {
    event.preventDefault()
    let {name, email, phone, address, company} = this.state
    this.props.editUser(this.props.selectedUserId, name, email, phone, address, company)
    this.setState({
      isDisabledSaveBtn: true,
      isVisibleCancelBtn: false
    })
  }
  render() {
    return (
        <Form onSubmit={this.handleSubmit} className="form-block">
          <Form.Group controlId="formBasicName">
              <Form.Label className="form__label">Name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" className="form__input" name="name" value={this.state.name} onChange={this.handleInputChange}/>
          </Form.Group>

          <Form.Group controlId="formBasicEmail">
            <Form.Label className="form__label">Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email address" className="form__input" name="email" value={this.state.email} onChange={this.handleInputChange}/>
          </Form.Group>

          <Form.Group controlId="formBasicNumber">
            <Form.Label className="form__label">Phone</Form.Label>
            <Form.Control type="tel" placeholder="Enter phone" className="form__input" name="phone" value={this.state.phone} onChange={this.handleInputChange}/>
          </Form.Group>

          <Form.Group controlId="formBasicAddress">
            <Form.Label className="form__label">Address</Form.Label>
            <Form.Control type="text" placeholder="Enter address" className="form__input" name="address" value={this.state.address} onChange={this.handleInputChange}/>
          </Form.Group>

          <Form.Group controlId="formBasicCompany">
            <Form.Label className="form__label">Company</Form.Label>
            <Form.Control type="text" placeholder="Enter company" className="form__input" name="company" value={this.state.company} onChange={this.handleInputChange}/>
          </Form.Group>
          <Button className={"button button__save " + (this.state.isDisabledSaveBtn ? "button--disabled" : "")} disabled={this.state.isDisabledSaveBtn} type="submit">
            Save
          </Button>
          <Button className="button button__cancel" type="button" style={{'visibility': this.state.isVisibleCancelBtn? 'visible': 'hidden'}} onClick={this.handleCancelBtn}>
            Cancel
          </Button>
        </Form>
    )
  }
}

UserForm.propTypes = {
  selectedUserId: PropTypes.string,
  selectedUser: PropTypes.object,
  getUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired
}
export default UserForm