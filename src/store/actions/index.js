import { 
  GET_USERS,
  GET_USER,
  EDIT_USER,
  SELECTED_USER_ID
} from '../constants/ActionTypes'
import imported_users from '../../user_data.js'

export const fetchUsers = () => ({
  type: GET_USERS,
  users: imported_users
})
export const getUsers = users => ({
  type: GET_USERS,
  users
})
export const getUser = id => ({
  type: GET_USER,
  id
})
export const selectedUserId = id => ({
  type: SELECTED_USER_ID,
  id
})
export const editUser = (id, name, email, phone, address, company) => ({
  type: EDIT_USER,
  id,
  name,
  email,
  phone,
  address,
  company
})