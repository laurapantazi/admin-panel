import { GET_USERS, GET_USER, EDIT_USER } from '../constants/ActionTypes'

export const users = (state = [] , action) => {
  switch (action.type) {
    case GET_USER:
      return state.filter(user => user.id === action.id)
    case GET_USERS: 
      return action.users
    case EDIT_USER:
      return state.map(user => 
        (user.id === action.id) ? {...user,
          name: action.name,
          company: action.company,
          email: action.email,
          phone: action.phone,
          address: action.address
        } : user)
    default:
      return state
  }
}