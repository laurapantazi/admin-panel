import { combineReducers } from 'redux'
import {users} from './users'
import {selectedUserId} from './user'

const tasksApp = combineReducers({
  users, selectedUserId
})

export default tasksApp