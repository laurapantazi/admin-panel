import UserList from '../components/UserList'
import {connect} from 'react-redux'
import {getUsers, fetchUsers, selectedUserId} from '../store/actions/index'

function mapStateToProps(state) {
  return {
    users: state.users
  }
}
export default connect(mapStateToProps, {getUsers, fetchUsers, selectedUserId})(UserList)