import UserForm from '../components/UserForm'
import {connect} from 'react-redux'
import {getUser, editUser} from '../store/actions/index'
import {getUserById} from '../store/selectors/index'

function mapStateToProps (state) {
  return {
    selectedUserId: state.selectedUserId,
    selectedUser : getUserById(state.users, state.selectedUserId)
  }
}
export default connect(mapStateToProps, {getUser, editUser})(UserForm)