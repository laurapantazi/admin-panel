import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'
import './style/main.scss'
import UserFormContainer from './containers/UserFormContainer'
import UserListContainer from './containers/UserListContainer'

function App() {
  return (
    <Container className="wrapper">
      <Row noGutters="true">
        <Col as="section" xs={4} sm={4} md={6}>
          <UserListContainer />
        </Col>
        <Col as="section" xs={8} sm={8} md={6}>
          <UserFormContainer />
        </Col>
      </Row>
    </Container>
  )
}

export default App
